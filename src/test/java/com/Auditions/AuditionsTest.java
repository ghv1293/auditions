package com.Auditions;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuditionsTest {

	@Test
	public void testPerformers() {
		
		String expected = "Came for Audition";
		
		Performers performers = new Performers();
		
		String actual = performers.perform("324 - performer");
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testDancers() {
		
		String expected = "tap dancer Came for Audition";
		
		Dancers dancers = new Dancers();
		
		String actual = dancers.perform("tap - 772 - dancer");
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testVocalistWithKey() {
		
		String expected = "Vocalist Came for Audition and Key is G";
		
		Vocalist vocalist = new Vocalist();
		
		String actual = vocalist.perform("I sing in the key of - G - 1191");
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testVocalistWithKeyAndVolume() {
		
		String expected = "Vocalist Came for Audition and Key is G at the volume 5";
		
		Vocalist vocalist = new Vocalist();
		
		String actual = vocalist.perform("I sing in the key of - G - at the volume 5 - 1245 ");
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testAudition() {
		
		boolean expected = true;

		
		Auditions auditions = new Auditions();
		
		auditions.enterAuditions("Performer", "654 - performer");
		auditions.enterAuditions("Performer", "655 - performer");
		auditions.enterAuditions("Performer", "656 - performer");
		auditions.enterAuditions("Performer", "657 - performer");
		
		auditions.enterAuditions("Dancer", "tap - 772 - dancer");
		auditions.enterAuditions("Dancer", "tap - 775 - dancer");
		
		auditions.enterAuditions("Vocalist", "I sing in the key of - G - at the volume 5 - 1245 ");
		
		boolean actual = auditions.startAuditions();
		
		assertEquals(expected, actual);
		
	}
	
	

}
