package com.Auditions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class Auditions {
	final static Logger logger = Logger.getLogger(Auditions.class);

	Map<String,String> allAuditions = new HashMap<String, String>();
	
	void enterAuditions(String performer, String input) {
		if("Performer".equals(performer)) {
			Performers performers = new Performers();
			allAuditions.put(input, performers.perform(input));
		}
		else if("Dancer".equals(performer)) {
			Dancers dancers = new Dancers();
			allAuditions.put(input, dancers.perform(input));
		}
		else if("Vocalist".equals(performer)) {
			Vocalist vocalist = new Vocalist();
			allAuditions.put(input, vocalist.perform(input));
		}
	}
	
	boolean startAuditions() {
		
		boolean completedAuditions = false;
		
        for (Entry<String,String> auditions : allAuditions.entrySet()){
        	logger.info("Input:"+auditions.getKey()+" ---->"+" Output:"+auditions.getValue());
        	completedAuditions = true;
        }
		
		
		
		return completedAuditions;
		
	}
}
